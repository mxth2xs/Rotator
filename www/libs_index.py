from os import path as os_path, name as os_name
from random import choice as choix_random
from json import load as json_load, dump as json_dump
from subprocess import PIPE as subprocess_PIPE, run as subprocess_run
import cgi
# from jeux.config import DRY as AVEC_OU_SANS_LEDS
if os_name != 'nt':
    from os import chown as os_chown

from os import environ
environ['DISPLAY'] = ':0'
environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'
