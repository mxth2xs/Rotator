#! /usr/bin/python3
#! C:\Users\Max\AppData\Local\Programs\Python\Python310\python.exe
from libs_index import *

def configuration_parametres() -> dict[str, dict]:
    """ Definitions des données fixes.
    """
    # Les paramètres fixes des programmes que l'utilisateur peut lancer.
    SETTINGS = {
        #!!! L'input 'mode_jeu' comprend : (Dry, difficulté, mode spécial)
        "Snake" : {
            "INPUTSFORMS" : ['nom_joueur',  "mode_jeu", "difficulte"],
            "HAS_OUTPUT" : True,
            "FORM" : [],
            "MODES" : [('D', 'B', 'A', 'S', 'S++'), ('M')],
            "DATAFILEPATH" : "",
            "GAMEPATH" : "snake.py",
        },
        
        "Labyrinthe" : {
            "INPUTSFORMS" : ['nom_joueur',  "mode_jeu", "difficulte"],
            "HAS_OUTPUT" : True,
            "FORM" : [],
            "MODES" : [(), ()],
            "DATAFILEPATH" : "",
            "GAMEPATH" : "labyrinthe.py",
        },

        "JeuDeLaVie" : {
            "INPUTSFORMS" : ['aleatoire'],
            "HAS_OUTPUT" : False,
            "FORM" : [],
            "MODES" : [('True', 'False')],
            "DATAFILEPATH" : "",
            "GAMEPATH" : "jeudelavie.py",
        },

        "JumpMan" : {
            "INPUTSFORMS" : ['nom_joueur',  "mode_jeu", "difficulte"],
            "HAS_OUTPUT" : True,
            "FORM" : [],
            "MODES" : [('D','S'), ()],
            "DATAFILEPATH" : "",
            "GAMEPATH" : "jumpman.py",
        },
    }

    # Ce champs ne peut être rentré qu'une fois le dictionnaire des paramètres créé.
    for jeu in SETTINGS:
        SETTINGS[jeu]["DATAFILEPATH"] = "./data/scores/" + jeu + ".json"

    return SETTINGS

def mise_a_jour_formulaire(SETTINGS, mode_jeu):
        # Tous les modules d'entrées (modules HTML).
    MODULES_INPUT = {
        "nom_joueur":"""
            <div class="form-group">
                <label for="playerName">Nom : </label>
                <input type="text" class="form-control" id="playerName" name="name" placeholder="Entrer un nom" value="{nom_joueur}">
            </div>""",
        "difficulte":"""
            <div class="form-group">
                <label for="diffSelect">Difficult&#233; : </label>
                <select class="form-control" id="diffSelect" name="diff">
                    <option value="{diff}" selected>{diff}</option>
                    {options_jeu}
                </select>
            </div>""",
        "mode_jeu":"""
            <div class="form-group">
                <label for="modeSelect">Mode : </label>
                <select class="form-control" id="modeSelect" name="mode">
                    <option value="" selected>Solo</option>
                    <option value="M">Multijoueur</option>
                </select>
            </div>""",
        "aleatoire":"""
            <div class="form-group">
                <label for="modeSelect">Mode : </label>
                <select class="form-control" id="modeSelect" name="mode">
                    <option value="True" selected>Al&#233;atoire</option>
                    <option value="False">Motif de la banque de donn&#233;es</option>
                </select>
            </div>"""
        
    }
    # Ces deux champs ne peuvent être rentrés une fois le dictionnaire des paramètres créé.
    for jeu in SETTINGS:
        # Préparer les options pour les modes en fonction du jeu.
        options = [f'<option value="{d}">{d}</option>' for d in SETTINGS[jeu]['MODES'][0] if d != mode_jeu[0]]
        SETTINGS[jeu]["FORM"] = [MODULES_INPUT[entree].format(diff=mode_jeu[0], options_jeu=options) if entree == 'difficulte' else MODULES_INPUT[entree] for entree in SETTINGS[jeu]["INPUTSFORMS"]]
        SETTINGS[jeu]["GAMEPATH"] = "./jeux/" + SETTINGS[jeu]["GAMEPATH"]
    return SETTINGS

def recup_donnees_JSON(filepath:str) -> dict:
    """ Fonction pour récupérer les données d'un fichier JSON.

    Args:
        filepath (str): Le chemin relatif vers le fichier.

    Returns:
        dict: Le contenu du fichier.
    """
    
    # Si le fichier de données n'existe pas encore.
    if os_path.isfile(filepath): 
        return json_load(open(filepath))
    # S'il existe, on charge les données.
    else:
        return {} 

def insert_donnees(donnees:dict, categorie:str, sous_categorie:str, nom_joueur:str, score:int) -> tuple[dict, str]:
    """Insérer les données du joueur dans le fichier de données correspondant.

    Args:
        donnees (dict): Les données du jeu sous forme de dictionnaire.
        categorie (str): La catégorie choisie par l'utilisateur.
        sous_categorie (str): La sous-catégorie choisie par l'utilisateur.
        nom_joueur (str): Le nom donné.
        score (int): Le score obtenu à l'issue de la partie.

    Returns:
        donnees, message: Les données misent à jour, le message destiné à l'utilisateur.
    """
    
    # On prépare le message de sortie pour la partie.
    message = f"Nom : {nom_joueur} | Score : {score}. "
    

    # Si le jeu à déja été lancé, on ajoute le nom du joueur 
    # dans les donnée du jeu pour éviter de donner un même nom 
    # à un autre utilisateur plus tard.
    if "Joueurs" in donnees: 
        if nom_joueur not in donnees["Joueurs"]:
            donnees["Joueurs"].append(nom_joueur)
    else:
        donnees["Joueurs"] = [nom_joueur]


    # La catégorie n'existe pas dans les données : on la créé.
    if categorie not in donnees:
        donnees[categorie] = {}
    
    # La sous-catégorie n'existe pas dans les données : on la créé.
    if sous_categorie not in donnees[categorie]:
        donnees[categorie][sous_categorie] = {}
    
    # Récupérer le score maximale avec cette configuration.
    highscore = max(donnees[categorie][sous_categorie].values(), default=0)

    # Comparer les score et ajouter un joueur meilleur 
    # que l'utilisateur si ce dernier a un nouveau record.
    if score > highscore:
        donnees[categorie][sous_categorie]['Max'] = score + 1
        message += "Presque le nouveau record ;) "

    # Le joueur n'a jamais joué dans la catégorie.
    if nom_joueur not in donnees[categorie][sous_categorie]:
        donnees[categorie][sous_categorie][nom_joueur] = score

    # Le joueur a battu son record.
    if score > donnees[categorie][sous_categorie][nom_joueur]:
        donnees[categorie][sous_categorie][nom_joueur] = score


    return donnees, message

def mise_a_jour_score(score:int, a_une_sortie:bool, nom_joueur:str, mode_jeu:list, filepath:str) -> str: 
    """ Mettre a jour le fichier de données, et récupérer le message de résultat.

    Args:
        score (int): Le score obtenu.
        a_une_sortie (bool): Si le jeu à une sortie.
        nom_joueur (str): Le nom donné au joueur.
        mode_jeu (str): Le mode choisi.
        filepath (str): Le chemin vers le fichier de données du jeu.

    Returns:
        str: Le message de résultat.
    """

    # Si le jeu a une sortie.
    if a_une_sortie:
        
        # Récupérer les données du jeu, la catégorie et sous-catégorie.
        donnees = recup_donnees_JSON(filepath)
        categorie, sous_categorie = mode_jeu

        # Mettre à jour les données et récupérer le message de résultat.
        donnees, message = insert_donnees(donnees, categorie, sous_categorie, nom_joueur, score)

        # Sauvegarder les données de jeu sous forme de fichier json.        
        with open(filepath, 'w') as f: 
            json_dump(donnees, f, ensure_ascii=False, indent=4)

        # Si l'on est sur la RPi, il est nécessaire de donner 
        # les permissions adaptées au fichier du jeu.
        if os_name != 'nt':
            os_chown(filepath, 33, 33)

        return message

    else:
        return ''

def recuperer_classement(a_une_sortie:bool, mode_jeu:list, nom_joueur:str, filepath:str) -> str:
    """ Récupérer le classement mis à jour pour le mode choisi.

    Args:
        a_une_sortie (bool): Si le jeu à une sortie.
        mode_jeu (str): Le mode choisi.
        nom_joueur (str): Le nom donné au joueur.
        filepath (str): Le chemin vers le fichier de données du jeu.

    Returns:
        str: Le message de résultat.
    """

    # Si le jeu a une sortie.
    if a_une_sortie:
        
        # Récupérer les données du jeu, la catégorie et sous-catégorie.
        donnees = recup_donnees_JSON(filepath)
        categorie, sous_categorie = mode_jeu
        donnees = donnees[categorie][sous_categorie]

        # Créer un dictionnaire répertoriant les joueurs par score.
        joueurs_par_score = {}
        for joueur, score in donnees.items():
            if score not in joueurs_par_score:
                joueurs_par_score[score] = [joueur]
            else:
                joueurs_par_score[score].append(joueur)

        # Faire une liste avec ce dictionnaire, et la trier.
        joueurs = [(k, v) for k, v in sorted(joueurs_par_score.items(), key=lambda item: item[0], reverse=True)]

        classement, i = "", 0

        # On affiche, au plus, les 5 premiers du classement.
        # La ligne avec le score du joueur est en gras sur un fond bleu.      
        while i < min(5, len(joueurs)):

            if nom_joueur in joueurs[i][1]:
                classement += f"""<tr style="background-color:rgba(1, 186, 239, 0.1);">
                                <th scope="row"><b>{i+1}</b></th>
                                <td><b>{", ".join(joueurs[i][1])}</b></td>
                                <td><b>{joueurs[i][0]}</b></td>
                            </tr>"""
            else:
                classement += f"""<tr>
                                <th scope="row">{i+1}</th>
                                <td>{", ".join(joueurs[i][1])}</td>
                                <td>{joueurs[i][0]}</td>
                            </tr>"""
            
            i += 1

        tableau = """
            <table>
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nom</th>
                        <th scope="col">Score</th>
                    </tr>
                </thead>
                <tbody>
                    {:}
                </tbody>
            </table>"""
        return tableau.format(classement)
    else:
        return ''

def generateur_nom(a_une_sortie, path:str) -> str:
    """Génétateur de nom aléatoire pour le joueur.
    ATTENTION, si toutes les combinaisons de noms sont prises, 
    La fonction sera bloquée dans une boucle infinie.
    Nombre de combinaisons = n_adj x n_noms.
    Pour l'instant, n_combinaisons = 30*146 = 4 380

    Args:
        path (str): Le chemin relatif vers le fichier de données du jeu.

    Returns:
        str: Le nom choisi.
    """
    # Si le jeu a une sortie.
    if a_une_sortie:
        
        # Récupérer les noms et les adjectifs pour créer un pseudo.
        with open(".//data//noms//noms.txt", "r") as fichier_noms:  
            noms = fichier_noms.read().split("\n")
        
        with open(".//data//noms//adjectifs.txt", "r") as fichier_adjs:  
            adjectifs = fichier_adjs.read().split("\n")
        
        # Données du jeu...
        donnees_du_jeu = recup_donnees_JSON(path)

        # Tant que le nom est déjà utilisé, on en re-génère un.
        choix = choix_random(adjectifs) + " " + choix_random(noms)
        if "Joueurs" in donnees_du_jeu:
            while choix in donnees_du_jeu["Joueurs"]:
                choix = choix_random(adjectifs) + " " + choix_random(noms)

        return choix
    else:
        return ''

def GET_donnees(SETTINGS:dict) -> tuple[str, bool, str, tuple[str]]:
    """Récupérer les données passer par la requête GET.

    Args:
        SETTINGS (dict): Les paramètres des jeux.

    Returns:
        jeu (str) : le nom du jeu.
        run (bool) : Si le jeu doit être lancé. 
        nom_joueur (str): Le nom donné au joueur.
        mode_jeu (tuple): Le mode choisi par le joueur. 
    """
    # Récupérer les entrées.
    form = cgi.FieldStorage()

    jeu = form.getfirst("jeu")
    run = form.getfirst("run", "False")
    nom_joueur = form.getfirst("name", generateur_nom(SETTINGS[jeu]["HAS_OUTPUT"], SETTINGS[jeu]["DATAFILEPATH"]))
    mode_jeu = (
        form.getfirst("diff", "D"), # La difficulté du jeu si elle est entrée.
        form.getfirst("mode", "")  # Le mode spécial s'il y en a un...
    )
    
    return jeu, run, nom_joueur, mode_jeu

def template_load(nom_du_jeu:str, template:str, sortie_jeu="", classement="", nom_joueur="", inputs_du_jeu="", mode_jeu=['', '']) -> str:
    """Récupérer une page HTML voulue avec les valeurs variables données.

    Args:
        nom_du_jeu (str): Le nom du jeu.
        template (str): Le nom de la page HTML souhaitée.
        sortie_jeu (str, optionel): Le message de sortie du jeu. Defaults to "".
        classement (str, optionel): Le classement obtenu. Defaults to "".
        nom_joueur (str, optionel): Le nom du joueur. Defaults to "".
        inputs_du_jeu (str, optionel): Les entrées nécessaires au jeu. Defaults to "".

    Returns:
        str: La page avec les variables entrées.
    """

    # template = template_setup.html ; template_playing.html
    body = open(f"./pages/{template}", 'r')
    body_content = body.read().format(nom = nom_du_jeu, inputs = inputs_du_jeu, output = sortie_jeu, classement=classement, nom_joueur=nom_joueur, mode=mode_jeu[1], diff=mode_jeu[0])
    body.close()

    with open("./pages/template_head_footer.html", 'r') as f:
        return f.read().format(nom = nom_du_jeu, body=body_content) 

def main():

    SETTINGS = configuration_parametres()
    jeu, run, nom_joueur, mode_jeu = GET_donnees(SETTINGS)
    SETTINGS = mise_a_jour_formulaire(SETTINGS, mode_jeu)

    # Si le jeu doit être lancé :
    if eval(run) == True :
        
        # On détermine les parametres et leur valeur pour le jeu en question.
        a_une_sortie = SETTINGS[jeu]["HAS_OUTPUT"]

        # Chemin vers le fichier des données pour le jeu
        filepath = SETTINGS[jeu]["DATAFILEPATH"]

        # Chemin vers le fichier du jeu
        gamepath = SETTINGS[jeu]["GAMEPATH"]

        
        # Si le jeu est lancé sur une machine Linux, 
        # l'élévation de privilège est nécessaire au lancement du jeu.
        if os_name == 'nt':
            command = ['python', gamepath] + [param for param in mode_jeu]
        else:
            command = ['sudo', 'python', gamepath] + [param for param in mode_jeu]
        
        # On lance le jeu comme subprocess (lancement bloquant).
        result = subprocess_run(command, stdout=subprocess_PIPE, stderr=subprocess_PIPE, universal_newlines=True)

        # Récupérer la sortie détaillée.
        debug_results = str([result.returncode,result.stdout, result.stderr])
        
        # Si le programme n'a pas eu de problèmes d'exécution, on récupère le score.
        if result.returncode == 0 and a_une_sortie:    
            score = int(result.stdout)
        else:
            score = -1
        
        # Mise à jour des fichiers, du classement.
        message_resultat = mise_a_jour_score(score, a_une_sortie, nom_joueur, mode_jeu, filepath)
        classement = recuperer_classement(a_une_sortie, mode_jeu, nom_joueur, filepath)
        if result.returncode != 0: # S'il y a eu une erreur, on prévoit d'afficher la sortie du jeu.
            classement += debug_results
        
        # Afficher la page de résultat.
        print(template_load(nom_du_jeu=jeu, template="template_playing.html", sortie_jeu=message_resultat, classement=classement, nom_joueur=nom_joueur, mode_jeu=mode_jeu))

    # Sinon, on donne la page de paramétrage pour le jeu.
    else:
        inputs = "\n".join(SETTINGS[jeu]["FORM"]).format(nom_joueur = nom_joueur)
        print(template_load(nom_du_jeu=jeu, template="template_setup.html", inputs_du_jeu=inputs))


if __name__ == "__main__":

    # Lancer la fonction principale, et, si erreur il y a, renvoyer ces dernières à la place.
    try:
        print("Content-type: text/html\n\n")
        main()
    except:
        cgi.print_exception()          
