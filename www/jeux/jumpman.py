import config
from libs_externes import *


class Game:
    """
    Jeu de plateforme extraordinaire !
    +--------------------------------+

    ### Attributs :
    game (bool) : Indique si la partie est en cours ou terminée.
    score (int) : Le score de la partie en cours.

    width, height (int,int) : nombre de LEDs/cases en largeur et hauteur.

    on_platform (bool) : Indique si le personnage est sur une plateforme ou non.
    first_map (bool) : Indique s'il s'agit du premier terrain de jeu.

    SUR_LEDS (bool) : Indique si le jeu se lance avec les LEDs activées.
    vitesse (float) : Vitesse de défilement du jeu.
    
    Perso (Objet Perso) : Le perso de la partie, créé avec la subclass 'Perso'.
    Map (Objet Map) : Les terrains du jeu, créés avec la subclass 'Map', et les plateformes immobiles.
    Modes (Objet Modes) : Les modes activés en jeu (avec ou sans LEDs), créés avec la subclass 'Modes'.
    Missiles (Objet Missiles) : Les missiles sur le terrain pour tuer le joueur, créés \\
    avec la subclass 'Missiles'.

    manette : La manette de jeu branchée.
    strand : Le panneau de LEDs branché.
    num_led (int) : Le nombre de LEDs du panneau.
    listener : thread qui écoute pour les entrées utilisateur.

    ### Méthodes :
    initmoteur() : Initialisation du moteur pour le jeu sur LEDs.
    adresseLED(x, y) : Obtenir le numéro de la led correspondant aux coordonnées (x ,y).
    affichageLED(x, y, color): Afficher un pixel sur les LEDs de la couleur donnée.
    drawGame() : Afficher le jeu avec ou sans LEDs.
    condition_mort(perso) : Vérifie si le personnage vérifie les conditions de mort du jeu.
    mort(perso_mort): Retire le personnage de la liste des joueurs.
    changeGame() : Change le terrain de jeu et retéléporte le personnage à un endroit prédéfini \
    sur le sol le plus bas de la nouvelle zone correspondant à la partie haute de l'ancienne zone. \
    Augmente le score du joueur de 1.
    key_press_clavier(key) : Permet de diriger les directions du personnage en fonction de l'entrée key.
    key_press_pygame() : Permet de diriger les directions du personnage avec la manette. 
    mainloop() : Boucle principale du jeu, gère le déplacement et les évenements d'une partie. \
    Affiche le score à la fin de la partie.
    """

    def __init__(self, options_jeu=("D", "")) -> None:
        self.game = True

        # Initialisation du score
        self.score = 0
        self.width, self.height = config.WIDTH-1, config.HEIGHT

        # Mesure indiquant si le personnage est sur une plateforme ou non
        self.on_platform = False
        # Mesure indiquant s'il s'agit du premier terrain de jeu
        self.first_map = True

        # Initialisation des modes
        self.SUR_LEDS = config.SUR_LEDS
        self.vitesse = 0.03

        # Initialisation du personnage
        self.perso = Personnage(self)

        # Initialisation du terrain et des plateformes
        self.map = Map(self)
        self.modes = Modes(self, options_jeu)

        # Initialisation pour un jeu lancé sur LEDs.
        if self.SUR_LEDS:

            # Manettes
            pygame.init()
            assert pygame.joystick.get_count() >= 1, f"Il n'y a pas de manette connectée !"
            self.manette = pygame.joystick.Joystick(0)
            self.manette.init()

            # Leds
            self.strand, self.num_led = config.strand, config.NUM_LED
            self.strand.show()

            # Moteur
            self.initMoteur()

        else:
            from pynput import keyboard
            self.listener = keyboard.Listener(on_press=self.key_press_clavier)
            self.listener.start()

            # Affichage pygame
            from lib.Pseudo_affichage import Pseudo_leds
            self.pseudo_leds = Pseudo_leds([self.width, self.height-1])

        # Execution de drawGame() : Actualisation des LEDs
        self.drawGame()

    def initMoteur(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(config.MOTOR_PIN, GPIO.OUT)
        self.p = GPIO.PWM(config.MOTOR_PIN, config.MOTOR_HZ)
        self.p.start(0)  # Initialization

    def adresseLED(self, x: int, y: int) -> int:
        """Méthode pour inverser à cause de la disposition en zigzag des LEDs. (lignes paires et impaires)
        Args:
            x (int): Abscisse de la led.
            y (int): Ordonné de la led
        """
        if y % 2 == 0: return ((y*22)+x)
        else: return ((y*22)+(21-x))

    def affichageLED(self, x, y, color=None):
        """Afficher un pixel sur les LEDs de la couleur donnée."""
        if   color == "MAP":  self.strand.setPixelColor(self.adresseLED(x,y), 0, 255, 0)
        elif color == "PERSO":   self.strand.setPixelColor(self.adresseLED(x,y), 0, 0, 255)
        elif color == "PLATFORM": self.strand.setPixelColor(self.adresseLED(x,y), 255, 0, 0)
        elif color == "MISSILES": self.strand.setPixelColor(self.adresseLED(x,y), 255, 0, 255)
        else: self.strand.setPixelColor(self.adresseLED(x,y), 0, 0, 0)

    def drawGame(self):
        """Afficher le jeu sur les LEDs."""

        # Récupérer la liste des coordonnées des missiles.
        missiles = self.modes.coordonnees_missiles()
        ground = self.map.NOLED_ground_leds
        platformes = self.map.NOLED_aerial_leds
        perso = self.perso.player_positions

        # Pour un jeu avec LEDs, on affiche tous les objets dessus.
        if self.SUR_LEDS:
            # Terrain
            for (x, y) in ground:
                self.affichageLED(x, y, "MAP")
            for (x, y) in platformes:
                self.affichageLED(x, y, "PLATFORM")
            # Personnage
            for (x, y) in perso:
                self.affichageLED(x, y, "PERSO")
            # Plateformes
            for (x, y) in missiles:
                self.affichageLED(x, y, "MISSILES")

            self.strand.show()

        else:

            game_status = {
                "pos1": perso,
                "map": ground,
                "aerial_map": platformes,
                "missiles_jump": missiles,
            }
            self.pseudo_leds.update(game_status)

    def condition_mort(self, perso) -> bool:
        obstacles = self.modes.coordonnees_missiles()

        # - Le personnage touche un missile.
        touche_obstacle = any(
            [obs in perso.player_positions for obs in obstacles])
        # - Le personnage tombe en dehors de l'écran de jeu.
        if self.perso.player_positions[1][1] < 0:
            touche_obstacle = True

        return touche_obstacle

    def mort(self):
        """Méthode pour la mort du personnage.

        Args:
            message (str): Le message en fonctionn de la mort.
        """
        self.game = False

        if self.SUR_LEDS:
            self.manette.quit()
            for i in range(self.num_led):
                self.strand.setPixelColor(i, 0, 0, 0)
            self.strand.show()
            pygame.quit()
            # Stop le moteur.
            self.p.stop()
            GPIO.cleanup()

        else:
            # Arrête d'écouter les touches entrées par le joueur pour arrêter le jeu
            self.listener.stop()

    def changeGame(self):
        """Change le terrain du jeu lorsque le personnage atteint une hauteur supérieure à celle \
        de la dernière plateforme."""

        if self.SUR_LEDS:
            for i in range(self.num_led):
                self.strand.setPixelColor(i, 0, 0, 0)
            self.strand.show()

        # Vérifier si le joueur vient de parcourir le premier terrain
        if self.first_map == True:
            self.first_map = False

        # Incrémente le score du joueur
        self.score += 1

        # Changement de direction pour que le personnage reste sur la dernière plateforme du terrain précédent
        self.perso.direction = 3
        self.perso.deplacement()

        # Nouveau terrain (sans sol)
        self.map = Map(self)

        # Liste temporaire de plateformes
        platform_temp_leds = []

        # Coordonnées de la première plateforme
        for i in range(len(self.map.NOLED_aerial_leds)-1):
            # Hauteur de plateforme la plus basse
            if self.map.NOLED_aerial_leds[i][-1] == 5 and self.map.NOLED_aerial_leds[i+1][0] == self.map.NOLED_aerial_leds[i][0]+1:
                platform_temp_leds.append(self.map.NOLED_aerial_leds[i])
            else:
                break

        # Téléporter le joueur sur la première plateforme nouvellement créée
        self.perso.player_positions = [[(platform_temp_leds[0][0]+platform_temp_leds[-1][0])//2,
                                        platform_temp_leds[0][1]+1], [(platform_temp_leds[0][0]+platform_temp_leds[-1][0])//2,
                                                                      platform_temp_leds[0][1]+2]]

    def key_press_clavier(self, key):
        """Méthode pour déterminer la direction du snake en fonction de l'entrée utilisateur. Si le jeu est multijoueur, séparer les inputs.
        Args:
        key (String[z,s,d,q]): Touches pour diriger le snake.
        """
        try:
            for c, i in [('z', 0), ('d', 1), ('q', 2)]:
                if (key.char == c):
                    self.perso.keys[i] = True
        except:
            pass

    def key_press_pygame(self):
        self.manette: pygame.joystick.Joystick
        for event in pygame.event.get():
            if event.type == pygame.JOYAXISMOTION:
                if event.joy == 0:
                    if event.axis == 0:
                        if self.manette.get_axis(event.axis) < -0.5:
                            self.perso.keys[1] = True  # GAUCHE
                        elif self.manette.get_axis(event.axis) > 0.5:
                            self.perso.keys[2] = True  # DROITE
                    elif event.axis == 1:
                        if self.manette.get_axis(event.axis) < -0.5:
                            self.perso.keys[0] = True  # HAUT

            if event.type == pygame.JOYBUTTONDOWN and event.button == 4:
                self.p.ChangeDutyCycle(config.ROTATION_BAS)
            elif event.type == pygame.JOYBUTTONDOWN and event.button == 5:
                self.p.ChangeDutyCycle(config.ROTATION_HAUT)
            elif event.type == pygame.JOYBUTTONUP and event.button == 4:
                self.p.ChangeDutyCycle(0)
            elif event.type == pygame.JOYBUTTONUP and event.button == 5:
                self.p.ChangeDutyCycle(0)

            # Stop
            elif event.type == pygame.JOYBUTTONDOWN and event.button == 1:
                self.mort()

    def mainloop(self):
        # Tant que le jeu est en cours :
        while self.game:
            # Attendre 0.1 seconde (Vitesse du jeu)
            time.sleep(self.vitesse)

            # Temporisation du tour qui s'incrémente de 1 à chaque rafraichissement
            self.modes.TEMPO_update()

            # - Missiles : Des missiles de 1x1 traversent l'écran aléatoirement.
            if self.modes.DIFF >= 1:
                self.modes.mode_missiles()

            # Récupérer la direction choisie par le joueur.
            if self.SUR_LEDS:
                self.key_press_pygame()

            # Déplacement et collisions du joueur avec le terrain et les plateformes:
            self.perso.direction_update()
            self.perso.deplacement()

            # Vérification des conditions de mort du personnage (collision de missiles, chute)
            if self.condition_mort(self.perso):
                self.mort()

            # Vérification de la hauteur du personnage pour changer le terrain
            if self.perso.player_positions[0][1] > self.map.NOLED_aerial_leds[-1][1] and \
                    (not self.perso.saut and not self.perso.saut_realise and not self.perso.chute):
                self.changeGame()

            self.drawGame()

        # Si la boucle se termine, le jeu l'est aussi, on renvoie alors le score.
        return self.score


class Personnage:
    """
    Personnage contrôlé par le joueur au cours de la partie.
    +--------------------------------+
    ### Attributs :
    game() : Instance de la classe Game.
    perso_id (int) : Identifiant du personnage.
    manette () : 
    player_positions (list) : Liste donnant les coordonnées du personnage en jeu.
    perso_leds (list) : Liste donnant les coordonnées à attribuer au personnage en jeu dans le \\
    cas où les LEDs sont éteintes.

    ### Méthodes :
    addnewpiece(self, direction, delete) : Ajouter des LEDs affichant le personnage en fonction \\
    de ses coordonnées.
    """
    id = 0

    def __init__(self, game) -> None:
        self.game = game
        self.game: Game
        # CharacterID
        self.perso_id = Personnage.id

        self.manette = None

        # Direction (immobile vers le bas)
        self.direction = 3
        self.direction_lag = [0, 0]
        # Positions
        self.player_positions = [
            [self.game.width//2, 2], [self.game.width//2, 3]] if self.game.SUR_LEDS else [[10, 2], [10, 3]]
        # - Touche appuyée
        self.keys = [False, False, False]
        self.key_breaker = {1: 2, 2: 1}

        # Variables de saut
        self.saut, self.saut_realise, self.chute = False, False, False
        self.valeur_saut, self.vmax_saut = 0, 6
        self.debut_saut = self.player_positions[0][1]
        self.max_saut = self.player_positions[1][1]+self.vmax_saut

    def deplacement(self):
        """
        Ajoute deux pixels à droite, à gauche, ou six pixels (ajoutés puis supprimés un par un) \\
        au-dessus du personnage (inclinés ou non selon le saut, droit ou oblique par un déplacement)
        :param direction: (0-3) Direction du mouvement du personnage.
        """
        if self.game.SUR_LEDS:  # Eteindre les deux leds du perso.
            for (x, y) in self.player_positions:
                self.game.affichageLED(x, y, None)

        if self.direction == 0:  # HAUT
            if self.player_positions[-1][1] + 1 >= self.game.height:
                self.valeur_saut = -1
                self.saut, self.saut_realise, self.chute = False, True, True
                self.direction = 3

            if self.chute == True:
                self.direction = 3
            else:
                if self.saut == False:
                    self.SAUT_update()
                self.saut = True
                self.valeur_saut = 1

        if self.saut == True:
            self.player_positions[0][1] += self.valeur_saut
            self.player_positions[1][1] += self.valeur_saut
            if self.player_positions[1][1] >= self.max_saut:
                self.valeur_saut = -1
                self.saut, self.saut_realise, self.chute = False, True, True
                self.direction = 3

            # DROITE
            if self.direction == 1 and self.player_positions[1][0] < self.game.map.NOLED_ground_leds[-2][0]-1:
                # seulement la coordonnée x change : x+1
                for i in self.player_positions:
                    i[0] += 1

                # Condition pour savoir si le personnage n'est pas sur une plateforme
                if [self.player_positions[0][0], self.player_positions[0][1]-1] not in self.game.map.NOLED_aerial_leds:
                    self.game.on_platform = False

                # Changement de direction pour que le saut continue
                self.direction = 0

            # GAUCHE
            elif self.direction == 2 and self.player_positions[1][0] > self.game.map.NOLED_ground_leds[1][0]:
                # seulement la coordonnée x change : x-1
                for i in self.player_positions:
                    i[0] += -1

                # Condition pour savoir si le personnage n'est pas sur une plateforme
                if [self.player_positions[0][0], self.player_positions[0][1]-1] not in self.game.map.NOLED_aerial_leds:
                    self.game.on_platform = False

                # Changement de direction pour que le saut continue de se réaliser
                self.direction = 0
        else:
            if self.direction == 1:
                # DROITE
                if self.player_positions[1][0] < self.game.map.NOLED_ground_leds[-2][0]-1:
                    # seulement la coordonnée x change : x+1
                    for i in self.player_positions:
                        i[0] += 1

                    # Condition pour savoir si le personnage n'est pas sur une plateforme
                    if [self.player_positions[0][0], self.player_positions[0][1]-1] not in self.game.map.NOLED_aerial_leds:
                        self.game.on_platform, self.chute = False, True
                        self.direction = 3
                        if self.game.first_map:
                            if self.player_positions[0][1] > self.game.map.NOLED_ground_leds[1][1]+1:
                                self.valeur_saut = -1
                            else:
                                self.valeur_saut = 0

                    # Condition pour savoir si le personnage entre en collision avec une plateforme
                    else:
                        self.valeur_saut = 0
                        self.game.on_platform = True
                        self.direction = 3

                # Si le personnage heurte un mur ou ne bouge plus
                else:
                    self.direction = 3

            elif self.direction == 2:
                # GAUCHE
                if self.player_positions[1][0] > self.game.map.NOLED_ground_leds[1][0]:
                    # seulement la coordonnée x change : x-1
                    for i in self.player_positions:
                        i[0] -= 1

                    # Condition pour savoir si le personnage n'est pas sur une plateforme
                    if [self.player_positions[0][0], self.player_positions[0][1]-1] not in self.game.map.NOLED_aerial_leds:
                        self.game.on_platform, self.chute = False, True
                        self.direction = 3
                        if self.game.first_map:
                            if self.player_positions[0][1] > self.game.map.NOLED_ground_leds[1][1]+1:
                                self.valeur_saut = -1
                            else:
                                self.valeur_saut = 0

                    # Condition pour savoir si le personnage entre en collision avec une plateforme
                    else:
                        self.valeur_saut = 0
                        self.game.on_platform = True
                        self.direction = 3

                # Si le personnage heurte un mur ou ne bouge pas
                else:
                    self.direction = 3

            if self.direction == 3:  # BAS
                self.player_positions[0][1] += self.valeur_saut
                self.player_positions[1][1] += self.valeur_saut

                # Si le personnage arrive sur une plateforme
                if self.player_positions[0] in self.game.map.NOLED_aerial_leds:
                    self.player_positions[0][1] += 1
                    self.player_positions[1][1] += 1
                    self.game.on_platform = True
                    # Réinitialisation des variables de saut
                    self.valeur_saut = 0
                    self.saut_realise = False

                # Condition pour savoir si le personnage doit arrêter d'être en état de chute
                if self.chute:
                    if self.game.first_map:
                        # Si le personnage tombe vers le sol et qu'il y a effectivement un sol
                        if self.player_positions[0][1] == self.game.map.NOLED_ground_leds[1][1]+1:
                            self.valeur_saut = 0
                            self.chute = False
                    if (self.player_positions[0] in self.game.map.NOLED_aerial_leds and self.saut_realise) or self.game.on_platform:
                        self.valeur_saut = 0
                        self.chute = False

                else:
                    if self.game.first_map:
                        if self.player_positions[0][1] > self.game.map.NOLED_ground_leds[1][1]+1:
                            self.valeur_saut = -1
                        else:
                            # Réinitialisation des variables de saut
                            self.valeur_saut = 0
                            self.saut_realise = False
                    else:
                        # Le personnage continue de chuter tant qu'il n'arrive pas sur une plateforme
                        self.valeur_saut = -1

    def direction_update(self):

        if self.direction_lag != [0, 0]:
            # Si la touche opposée à celle pressée est pressée,
            if self.keys[self.key_breaker[self.direction_lag[0]]]:
                # On  arrête le lag.
                self.keys[self.direction_lag[0]] = False
                self.direction_lag = [0, 0]
            else:
                if self.direction_lag[1] == 3:
                    self.keys[self.direction_lag[0]] = False
                    self.direction_lag = [0, 0]
                else:
                    self.direction_lag[1] += 1

        for x in range(len(self.keys)):
            if self.keys[x] == True:
                # - ...changer à la direction associée,
                self.direction = x
                if x in [1, 2]:
                    if self.direction_lag == [0, 0]:
                        self.direction_lag = [x, 0]
                    else:
                        pass
                else:
                    # - ...désactiver la touche
                    self.keys[x] = False

    def SAUT_update(self):
        # Réinitialisation des coordonnées minimales/maximales du saut
        self.debut_saut = self.player_positions[0][1]
        self.max_saut = self.player_positions[1][1]+self.vmax_saut


class Map:
    """
    Terrain et plateformes sur lequels jouera le joueur au cours de la partie.
    """

    def __init__(self, game):
        self.game = game
        self.game: Game
        # Positions
        self.NOLED_ground_leds = []
        self.NOLED_aerial_leds = []

        # - lEDs du Terrain (Coordonnées)
        self.affichage_leds = []

        # Terrain immobile aérien
        for hauteur in range(5, 41, 5):
            platform1_x, platform2_x = random.randint(
                4, 12), random.randint(4, 12)
            # Vérifier si les deux valeurs sont égales pour éviter un nombre trop conséquent de plateformes confondues
            if platform1_x == platform2_x and platform1_x >= 8:
                platform1_x -= 4
            elif platform1_x == platform2_x and platform1_x <= 7:
                platform1_x += 4
            # Création des plateformes
            for longueur in range(random.randint(3, 6)):
                self.NOLED_aerial_leds.append([platform1_x+longueur, hauteur])
            for longueur in range(random.randint(3, 6)):
                self.NOLED_aerial_leds.append([platform2_x+longueur, hauteur])

        # Terrain immobile terrestre (sol)
        if self.game.first_map:
            for x in range(18):
                self.NOLED_ground_leds.append([x+2, 0])
                self.NOLED_ground_leds.append([x+2, 1])

        # Terrain immobile terrestre (murs)
        for y in range(self.game.height):
            self.NOLED_ground_leds.append([0, y])
            self.NOLED_ground_leds.append([1, y])
            self.NOLED_ground_leds.append([20, y])
            self.NOLED_ground_leds.append([21, y])


class Missile:
    """Une classe représentant un missile dans le jeu.

    Attributes:
    ----------
    - `x` : int
        - La position horizontale du missile sur le terrain.
    - `y` : int
        - La position verticale du missile sur le terrain.
    - `sens` : int
        - La direction du missile, soit 1 pour aller vers la droite et -1 pour aller vers la gauche.

    Methods:
    -------
    - ``position_invalide()`` -> bool
        - Détermine si le missile est arrivé de l'autre côté du terrain, selon son sens.
    - ``deplacer()`` -> None:
        - Déplace le missile d'une case dans la direction spécifiée par l'attribut `sens`.
    """

    def __init__(self, x: int, y: int, sens: int) -> None:
        self.x, self.y = x, y
        self.sens = sens

    def position_invalide(self) -> bool:
        """Déterminer si le missile est arrivé de l'autre coté du terrain, selon son sens."""
        return self.sens == -1 and self.x <= 2 \
            or (self.sens == 1 and self.x >= 19)

    def deplacer(self) -> None:
        """Déplacer un missile."""
        self.x += self.sens


class Modes:
    """
    Missiles du terrain tuant le joueur s'il les touche.
    """

    def __init__(self, game: Game, options_jeu: tuple):
        self.game = game
        dico_difficultes = {'D': 0, 'S': 1}
        self.DIFF = dico_difficultes[options_jeu[0]]
        self.TEMPO_tour = 0
        # - lEDs des missiles :
        self.missiles = []
        self.depart_par_sens = {1: 2, -1: 19}

    def TEMPO_update(self) -> None:
        "Incrémentation du compteur de tours."
        self.TEMPO_tour += 1

    def mode_missiles(self) -> None:
        """Gérer les missiles de la partie."""
        self.missiles: list[Missile]

        # Mise à jour de tous les missiles.
        for m in self.missiles:
            # Si un missile à terminé son parcours, on l'enlève.
            if m.position_invalide():
                self.supprimer_missile(m)
            # Sinon, on le déplace.
            else:
                self.deplacer_missile(m)

        # Créer des missiles à 20% de chance.
        if random.randint(1, 5) == 1:
            # Une chance sur deux pour son sens de parcours.
            sens = random.choice([-1, 1])
            height_range = [i for i in range(
                5, self.game.height-1) if i % 5 not in [0, 1]]
            self.missiles.append(
                Missile(self.depart_par_sens[sens], random.choice(height_range), sens))

    def supprimer_missile(self, m: Missile) -> None:
        """Supprimer un missile donné."""
        if self.game.SUR_LEDS:
            # Supprimer l'affichage sur LEDs du missile.
            self.game.affichageLED(m.x, m.y, None)
        # Supression de la liste des missiles.
        self.missiles.remove(m)

    def deplacer_missile(self, m: Missile):
        """Déplacer un missile."""
        if self.game.SUR_LEDS:
            # Supprimer l'affichage sur LEDs du missile.
            self.game.affichageLED(m.x, m.y, None)
        m.deplacer()

    def coordonnees_missiles(self) -> list[list[int, int]]:
        """Récupérer les coordonnées de tous les missiles."""
        return [[m.x, m.y] for m in self.missiles]


def JUMPMANGAME():  # AJOUTER LA DIFFICULTÉ EN PARAMETRE SUR TKINTER
    import sys
    option = sys.argv[1:]
    if option:
        difficulte, mode = option
        game = Game((difficulte, mode))
    else:
        game = Game(("D", ""))
    print(game.mainloop())


if __name__ == "__main__":
    JUMPMANGAME()