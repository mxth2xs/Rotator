# import importlib
import config
import eteint
from libs_externes import *


class JeuDeLaVie:
    """
    Jouez au jeu de la vie... Ou de la mort ! Un premier pas avant le labyrinthe...

    Attributs:
    ----------
    - `width`, `height` : int, int
        - Largeur et hauteur du panneau de LEDs.
    - `SUR_LEDS` : bool
        - Jouer avec ou sans les leds.
    - `game` : bool
        - Continuer à jouer au jeu.
    - `vitesse` : float
        - Vitesse du jeu, soit le temps entre chaque rafraîchissement.
    - `mode` : str
        - Le mode choisi par l'utilisateur : 'aleatoire' ou 'banque'.
        Note : Peut aussi être changé à 'favoris' une fois en partie.
    - `en_vie`, `mort` : int, int
        - Valeurs associées aux états possible pour chaque pixel..
    - 'pattern` : list
        - Modèle simplifié de la structure des leds.

    #### Si les LEDs sont branchées :
        - `strand` : NeoPixel_raspberry 
            - Séquence de neopixels, soit l'ensemble des LEDs du panneau.
        - `num_led` : int
            - Nombre de LEDs du panneau.
        - `manette` : pygame.joystick.Joystick
            - La manette pour contrôler le jeu.
    #### Sinon :
        - `listener` : keyboard.Listener
            - Instance pour récupérer les entrées clavier.
        - `pseudo_leds` : Pseudo_leds
            - Instance pour simuler les LEDs par un jeu pygame.

    Méthodes:
    ---------
    - ``adresseLED(x: int, y: int)`` -> int
        - Obtenir le numéro de la led correspondant aux coordonnées (x,y).
    - ``afficherSurLed(x: int, y: int, etat:int)`` -> None
        - Afficher un pixel sur les LEDs avec la couleur donnée.
    - ``initMoteur()`` -> None:
        - Initialise le moteur.
    - ``pattern_creation(mode: str)`` -> list
        - Création du pattern de départ s'il est aléatoire, génération à partir d'un fichier sinon.
    - ``calculer_nouvel_etat()`` -> None
        - Calculer l'état de chaque led en fonction de l'état de ses voisines.
    - ``nouveau_motif()`` -> None
        - Choisir un nouveau motif et recommencer l'évolution à partir de ce dernier.    
    - ``sauvegarder_favoris()`` -> None:
        - Sauvegarder un motif dans le fichier des favoris.    
    - ``key_press_manette()`` -> None:
        - Gère les entrées manette pour diriger le moteur et changer de motifs.
    - ``key_press_clavier()`` -> None:
        - Gère les entrées clavier pour changer de motifs dans le cas où on n'utilise pas la borne.
    - ``stop()`` -> None:
        - Arrête le jeu et réinitialise tous les composants.
    - ``mainloop()`` -> int
        - Code principal du jeu.
    """
    PATH_DB_CONFIGURTATIONS = path.dirname(
        path.abspath(__file__)) + '/../data/cells/'
    PATH_MOTIFS_FAVORIS = path.dirname(
        path.abspath(__file__)) + '/../data/jdv_favoris.txt'

    def __init__(self, mode=True):
        """Args:
            aleatoire (bool): Si la position de départ est aléatoire ou non.
        """
        self.width, self.height = config.WIDTH, config.HEIGHT
        self.SUR_LEDS = config.SUR_LEDS
        self.game = True
        self.vitesse = 0.06
        self.mode = 'aleatoire' if mode else 'banque'

        # Etats possibles pour chaque pixel.
        self.en_vie = 1
        self.mort = 0

        # Génération du motif de départ.
        self.pattern = self.pattern_creation(self.mode)

        # Initialisation pour un jeu lancé sur leds.
        if self.SUR_LEDS:

            # Manette
            pygame.init()
            self.nombre_manettes = pygame.joystick.get_count()
            assert self.nombre_manettes >= 1, "Il n'y a pas de manettes branchées."
            self.manette = pygame.joystick.Joystick(0)
            self.manette.init()

            # Leds
            self.strand, self.num_led = config.strand, config.NUM_LED
            self.strand.show()

            # Moteur
            self.initMoteur()

        else:

            # Sans les LEDs, on joue à l'aide d'un clavier et d'une simulation pygame.
            from pynput import keyboard
            # Listener pour les entrées clavier.
            self.listener = keyboard.Listener(on_press=self.key_press_clavier)
            self.listener.start()

            # Affichage pygame (sans leds)
            from lib.Pseudo_affichage import Pseudo_leds
            self.pseudo_leds = Pseudo_leds([self.width-1, self.height-1])

    def adresseLED(self, x: int, y: int) -> int:
        """Obtenir le numéro de la LED de coordonnées (x,y).

        Args:
            x (int): Abscisse de la led.
            y (int): Ordonné de la led.

        Returns:
            int: Numéro de la LED sur le panneau.
        """
        if y % 2 == 0:
            return ((y*self.width)+x)
        else:
            return ((y*self.width)+(self.width-1-x))

    def afficherSurLed(self, x: int, y: int, etat=0) -> None:
        """Afficher un pixel sur les LEDs avec la couleur donnée."""
        if etat == 0:  # Mort -> Pas d'affichage.
            r, v, b = (0, 0, 0)
        else:  # En vie -> Affichage en vert.
            r, v, b = (0, 255, 0)
        self.strand.setPixelColor(self.adresseLED(x, y), r, v, b)

    def initMoteur(self) -> None:
        """initialisation du moteur"""
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(config.MOTOR_PIN, GPIO.OUT)
        self.p = GPIO.PWM(config.MOTOR_PIN, config.MOTOR_HZ)
        self.p.start(0)  # Initialisation

    def pattern_creation(self, mode='aleatoire') -> list:
        """Création du motif aléatoire.

        Args:
            aleatoire (bool): Si la position de départ est aléatoire ou non.

        Returns:
            list[list]: Le tableau décrivant la couleur de chaque LED.
        """

        # Générer aléatoirement le motif.
        # en proposant un tableau content l'état de chaque pixel (vie/mort).
        # La probabilité qu'un pixel soit en vie est de 20% [ 1/card([0,1,2,3,4]) ].
        if mode == 'aleatoire':
            pattern = [[self.en_vie if random.randint(0, 4) == 0 else self.mort for _ in range(self.width)] for _ in range(self.height)]

        # Sinon, récupérer les motifs prets, dans le dossier /data/cells (fait par nous ou de la banque de fichier)
        elif mode in ['banque', 'favoris']:

            if mode == 'banque':
                # Séléction aléatoire d'un motif de la banque de données.
                PATH = JeuDeLaVie.PATH_DB_CONFIGURTATIONS
                self.fichier_choisi = random.choice(listdir(PATH))
                PATH = PATH + self.fichier_choisi

            else:
                # Des milliers de configurations,
                # nous en avons séléctionnées quelques unes !

                # Sélection puis lecture du fichier contenant nos motifs favoris !
                PATH = JeuDeLaVie.PATH_MOTIFS_FAVORIS

                with open(PATH) as f:
                    fichier = f.read().split('\n')

                # Sélection aléatoire d'un de nos motifs favoris.
                self.fichier_choisi = random.choice(fichier)
                PATH = JeuDeLaVie.PATH_DB_CONFIGURTATIONS + self.fichier_choisi
            # Importation du motif
            with open(PATH) as f:
                gamefile = f.read().split('\n')

            # Supression des lignes de commentaire du fichier.
            gamefile = [ligne for ligne in gamefile if ligne[0] != '!']

            # Calcul de marges pour centrer le motif : en abscisse et en ordonnées
            # (ce dernier n'étant important que pour la simulation, de fait de l'infinie ordonnée du Rotator)
            x_margin = (self.width - len(gamefile[0]))
            y_margin = (self.height - len(gamefile))
            x_margins = (x_margin//2, x_margin//2 + x_margin % 2)
            y_margins = (y_margin//2, y_margin//2 + y_margin % 2)

            # Modification du fichier par ajout des marges (en haut et en bas du motif).
            gamefile = ['.'*self.width]*y_margins[0] + gamefile + ['.'*self.width]*y_margins[1]

            # Modification du fichier par ajout des marges (gauche et droite du motif).
            for i in range(len(gamefile)):
                gamefile[i] = '.'*x_margins[0] + gamefile[i] + '.'*x_margins[1]

            # Création du motif en fonction des valeur données :
            # - un 'O' correspond à un pixel en vie,
            # - un '.' à un pixel mort.
            pattern = [[self.en_vie if gamefile[ligne][col] == 'O' else self.mort for col in range(self.width)] for ligne in range(self.height)]

        # Renvoyer le motif prêt.
        return pattern

    def calculer_nouvel_etat(self) -> None:
        """En s'appuyant sur les règles du jeu de la vie, 
        calcul le nouvel état du terrain. 
        (Comptabilisation du nombre de LEDs autour de chaque LED)
        """
        avenir_leds = {}

        for x in range(self.height):
            for y in range(self.width):

                # Le terrain est cylindrique, donc, par exemple,
                # - pour la première ligne (0), on regarde la ligne 1 et la ligne (0-1)%44=43.
                # - pour la dernière ligne (43), on regarde la ligne 42 et la ligne (43+1)%44=0.
                ligne_precedente = (x-1) % self.height
                ligne_suivante = (x+1) % self.height

                # Initialisation du nombre de LEDs autour de la LED en question.
                nb_led = 0

                # On se place dans le cas où j est différent de 0 pour calculer
                # les leds "GAUCHE" de la led. Dans le cas où j = 0, ces leds n'existent pas.
                if y != 0:
                    if self.pattern[ligne_precedente][y-1] == self.en_vie:  # HAUT
                        nb_led += 1

                    if self.pattern[x][y-1] == self.en_vie:  # MILIEU
                        nb_led += 1

                    if self.pattern[ligne_suivante][y-1] == self.en_vie:  # BAS
                        nb_led += 1

                # On se place dans le cas où j est différent de 21 pour calculer
                # les leds "DROITE" de la led. Dans le cas où j = 21, ces leds n'existent pas.
                if y != self.width-1:
                    if self.pattern[ligne_precedente][y+1] == self.en_vie:  # HAUT
                        nb_led += 1

                    if self.pattern[x][y+1] == self.en_vie:  # MILIEU
                        nb_led += 1

                    if self.pattern[ligne_suivante][y+1] == self.en_vie:  # BAS
                        nb_led += 1

                # On calcule les leds toujours existentes, c'est à dire les leds supérieurs et inférieurs.
                if self.pattern[ligne_precedente][y] == self.en_vie:  # HAUT
                    nb_led += 1

                if self.pattern[ligne_suivante][y] == self.en_vie:  # BAS
                    nb_led += 1

                # Sauvegarde de l'avenir de la cellule.
                if nb_led == 3:  # La cellule naît
                    avenir_leds[(x, y)] = True

                elif nb_led == 2:  # La cellule reste dans son état actuel
                    pass

                elif self.pattern[x][y] != self.mort:  # La cellule meurt
                    avenir_leds[(x, y)] = False

        # Mise à jour du motif.
        for key, item in avenir_leds.items():
            if item == True:
                self.pattern[key[0]][key[1]] = self.en_vie
            else:
                self.pattern[key[0]][key[1]] = self.mort

    def nouveau_motif(self) -> None:
        """Choisir un nouveau motif et recommencer l'évolution à partir de ce dernier.
        """
        self.pattern = self.pattern_creation(self.mode)

    def sauvegarder_favoris(self) -> None:
        """Sauvegarder un motif dans le fichier des favoris.
        ATTENTION : Cette fonctionnalité n'est proposée qu'avec la simulation.
        """
        with open(JeuDeLaVie.PATH_MOTIFS_FAVORIS, 'r') as f:
            data = f.read()
        data = data.split('\n')
        if self.fichier_choisi not in data:    
            with open(JeuDeLaVie.PATH_MOTIFS_FAVORIS, 'w') as f:
                f.write('\n'.join(data) + '\n' + self.fichier_choisi)
        print(self.fichier_choisi)

    def key_press_manette(self) -> None:
        """Gérer les entrées manette pour la rotation du ROTATOR."""
        for event in pygame.event.get():
            # Moteur.
            if event.type == pygame.JOYBUTTONDOWN and event.button == 4:
                self.p.ChangeDutyCycle(config.ROTATION_HAUT)
            elif event.type == pygame.JOYBUTTONDOWN and event.button == 5:
                self.p.ChangeDutyCycle(config.ROTATION_BAS)
            elif event.type == pygame.JOYBUTTONUP and event.button == 4:
                self.p.ChangeDutyCycle(0)
            elif event.type == pygame.JOYBUTTONUP and event.button == 5:
                self.p.ChangeDutyCycle(0)

            # Stopper le jeu.
            elif event.type == pygame.JOYBUTTONDOWN and event.button == 1:
                self.game = False

            # Changer le mode :
            elif event.type == pygame.JOYBUTTONDOWN and event.button == 2:
                self.mode = 'aleatoire'
                self.nouveau_motif()
            elif event.type == pygame.JOYBUTTONDOWN and event.button == 3:
                self.mode = 'banque'
                self.nouveau_motif()
            elif event.type == pygame.JOYBUTTONDOWN and event.button == 0:
                self.mode = 'favoris'
                self.nouveau_motif()

    def key_press_clavier(self, key) -> None:
        """Gérer les entrées clavier.
        Note : la syntaxe 'try... except' permet à l'utilisateur d'utiliser toute 
        autre touche du clavier sans se soucier de générer une erreur sur le programme.
        (key n'a pas toujours l'attribut 'char')
        """
        try:
            if (key.char == 'e'):
                self.mode = 'aleatoire'
                self.nouveau_motif()

            elif (key.char == 'r'):
                self.mode = 'banque'
                self.nouveau_motif()

            elif (key.char == 't'):
                self.mode = 'favoris'
                self.nouveau_motif()

            elif (key.char == 'o') and self.mode == 'banque':
                self.sauvegarder_favoris()

            elif (key.char == 'q'):
                self.game = False
        except:
            pass

    def stop(self) -> None:
        """Quitter le jeu, terminer les process appelés: moteur, manettes, LEDs.
        Note : Seulement utilisé avec les LEDs.
        """
        eteint.eteint()
        self.manette.quit()
        pygame.quit()
        self.p.stop()
        GPIO.cleanup()

    def mainloop(self) -> None:
        """Code principal du jeu."""

        # Le rajout d'un indice permet de limiter le nombre de génération
        # en cas de problème et d'éviter une génération infinie.
        i = 0
        while i < 10000 and self.game == True:
            time.sleep(self.vitesse)

            # Si le jeu est lancé *SANS* les LEDs, on utilise la simulation pygame.
            if not self.SUR_LEDS:

                # On retient dans une liste les coordonnées des LEDs en vie (coloriées).
                etat_jeu = {'pos1': [[x, y] for y in range(self.height) for x in range(self.width) if self.pattern[y][x] == self.en_vie]}

                # Mettre à jour la simulation avec le dictionnaire.
                self.pseudo_leds.update(etat_jeu)

            # Si le jeu est lancé *AVEC* les LEDs...
            else:
                # Récupérer les entrées de l'utilisateur et bouger le cylindre.
                self.key_press_manette()

                # Afficher chaque LED sur le panneau, avec la couleur donnée dans le tableau.
                for y in range(self.height):
                    for x in range(self.width):
                        self.afficherSurLed(x, y, etat=self.pattern[y][x])
                self.strand.show()

            self.calculer_nouvel_etat()
            i += 1

        # Lorsque le jeu se termine,  éteindre tout.
        if self.SUR_LEDS:
            self.stop()

        # Ce jeu n'a pas de sortie.
        return


def JEUDELAVIE():
    if sys_argv[1:]:

        mode_jeu = sys_argv[1:]
        game = JeuDeLaVie(eval(mode_jeu[1]))
    else:
        game = JeuDeLaVie()

    game.mainloop()


if __name__ == "__main__":
    JEUDELAVIE()
