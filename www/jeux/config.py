from dotenv import load_dotenv
from os import getenv, environ

environ['DISPLAY'] = ':0'
environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'


load_dotenv()

#Récupérer les données du fichier .env
SUR_LEDS=eval(getenv('SUR_LEDS'))
BOARD=eval(getenv('BOARD'))
HEIGHT=int(getenv('HEIGHT'))
WIDTH=int(getenv('WIDTH'))
HEIGHT_SCREEN=int(getenv('HEIGHT_SCREEN'))
WIDTH_SCREEN=int(getenv('WIDTH_SCREEN'))
NUM_LED = HEIGHT*WIDTH
PIXEL_PIN=str(getenv('PIXEL_PIN'))
BRIGHTNESS=int(getenv('BRIGHTNESS'))
ORDER=str(getenv('ORDER'))
COM=str(getenv('PORT'))
MOTOR_PIN=int(getenv('MOTOR_PIN'))
MOTOR_HZ=int(getenv('MOTOR_HZ'))
ROTATION_HAUT=float(getenv('ROTATION_HAUT'))
ROTATION_BAS=float(getenv('ROTATION_BAS'))


if SUR_LEDS:
    #CONFIGURATION DU PROGRAMME POUR UN RASPBERRY
    if BOARD == True:
            from lib.neopixel_raspberry import NeoPixel_raspberry
            strand = NeoPixel_raspberry(PIXEL_PIN, NUM_LED, brightness=BRIGHTNESS, auto_write=False, pixel_order=ORDER)
            MOTOR_SPEED_UP=7 #Vitesse du moteur vers le haut
            MOTOR_SPEED_DOWN=8.2 #Vitesse du moteur vers le bas


    #CONFIGURATION DU PROGRAMME POUR UN ARDUINO
    else:
        from lib.neopixel_arduino import NeoPixel_arduino
        from lib.servo import Servo
        NeoPixel = NeoPixel_arduino
        strand = NeoPixel(COM) #Choisissez le port série où votre Arduino est connecté. Cette information est visible sur le logiciel Arduino IDE
        strand.show()
        servo = Servo()
        MOTOR_SPEED_UP = 0.01
        MOTOR_SPEED_DOWN = 20
