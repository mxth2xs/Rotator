import config
from libs_externes import *


class GAME:
    """Créer un jeu Snake!

    Attributs:
    ----------
    - `score` : int
        - Score de la partie en cours.
    - `width` : int
        - Largeur du panneau de LEDs.
    - `height` : int 
        - Hauteur du panneau de LEDs.
    - `vitesse` : float
        - Vitesse du jeu, soit le temps entre chaque rafraîchissement.
    - `modes` : Modes
        - Instance pour les modes de la partie.
    - `joueurs` : list[Snake]
        - Instances pour le snake de chaque joueur.
    - `joueurs_en_vie` : list[bool]
        - Indique si le joueur d'ID i est en vie.
    - `food` : Food
        - Instance pour la nourriture de la partie.

    #### Si les LEDs sont branchées :
        - `strand` : NeoPixel_raspberry 
            - Séquence de neopixels, soit l'ensemble des LEDs du panneau.
        - `num_led` : int
            - Nombre de LEDs du panneau.
        - `manettes` : list[pygame.joystick.Joystick]
            - Les manettes pour contrôler les Snakes.
    #### Sinon :
        - `listener` : keyboard.Listener
            - Instance pour récupérer les entrées clavier.
        - `pseudo_leds` : Pseudo_leds
            - Instance pour simuler les LEDs par un jeu pygame.

    Méthodes:
    ---------
    - ``adresseLED(x: int, y: int)`` -> int
        - Obtenir le numéro de la led correspondant aux coordonnées (x,y).
    - ``afficherSurLed(x: int, y: int, color)`` -> None
        - Afficher un pixel sur les LEDs avec la couleur donnée.
    - ``dessiner()`` -> None
        - Afficher le jeu avec ou sans LEDs.
    - ``condition_mort(snake: Snake)`` -> bool
        - Déterminer si un snake donné doit mourir.
    - ``mort(snake_mort: Snake)`` -> None
        - Gérer la mort d'un snake.
    - ``snake_mange_food(snake: Snake, xfood: int, yfood: int)`` -> bool
        - Gérer les interactions entre le snake et la nourriture.
    - ``key_press_clavier(key: keyboard.Key)`` -> None
        - Gérer les entrées clavier pour diriger les snakes.
    - ``key_press_pygame()`` -> None:
        - Gère les entrées manette pour diriger les Snakes.
    - ``mainloop()`` -> int
        - Code principal du jeu.
    - ``initMoteur()`` -> None
        - Initialisation du moteur.
    """

    def __init__(self, options_jeu=("D", "")) -> None:
        """Args:
            options_jeu (tuple): 
            - [0] : le jeu est-il lancé avec les LEDs ? Par défaut : False
            - [1] : La difficulté du jeu. Par défaut : D
            - [2] : Multijoueurs ? Par défaut : "" (non).
        """
        # Initialisation du score, des dimentions du terrain,
        # de la vitesse du jeu (= temps entre chaque rafraichissement).
        self.score = 0
        self.width, self.height = config.WIDTH-1, config.HEIGHT
        self.vitesse = 0.1

        # Initialisation des instances utilisées (cf. docstrings).
        self.modes = Modes(self, options_jeu)
        self.joueurs = [Snake(self) for _ in range(self.modes.NB_JOUEURS)]
        self.joueurs_en_vie = [True]*len(self.joueurs)
        self.food = Food(self)

        # Initialisation pour un jeu lancé sur leds.
        if self.modes.SUR_LEDS:

            # Manettes
            pygame.init()
            self.nombre_manettes = pygame.joystick.get_count()
            self.manettes = [None]*self.nombre_manettes
            assert self.nombre_manettes >= self.modes.NB_JOUEURS, f"Il n'y a pas autant de manettes (={self.nombre_manettes}) que de joueurs (={self.modes.NB_JOUEURS}) !"

            for i in range(self.modes.NB_JOUEURS):
                self.manettes[i] = pygame.joystick.Joystick(i)
                self.manettes[i].init()

            # Leds
            from config import strand, NUM_LED
            self.strand, self.num_led = strand, NUM_LED
            self.strand.show()

            # Moteur
            self.initMoteur()

        else:
            # Sans les LEDs, on joue à l'aide d'un clavier et d'une simulation pygame.
            from pynput import keyboard
            # Listener pour les entrées clavier.
            self.listener = keyboard.Listener(on_press=self.key_press_clavier)
            self.listener.start()

            # Affichage pygame
            from lib.Pseudo_affichage import Pseudo_leds
            self.pseudo_leds = Pseudo_leds([self.width, self.height-1])

        # Actualisation des LEDs.
        self.dessiner()

    def adresseLED(self, x: int, y: int) -> int:
        """Obtenir le numéro de la LED de coordonnées (x,y).

        Args:
            x (int): Abscisse de la led.
            y (int): Ordonné de la led.

        Returns:
            int: Numéro de la LED sur le panneau.
        """
        if y % 2 == 0: return ((y*config.WIDTH)+x)
        else: return ((y*config.WIDTH)+(config.WIDTH-1-x))

    def afficherSurLed(self, x: int, y: int, color=None) -> None:
        """Afficher un pixel sur les LEDs avec la couleur donnée."""
        if color == "SNAKE":
            self.strand.setPixelColor(self.adresseLED(x, y), 0, 255, 0)
        elif color == "FOOD":
            self.strand.setPixelColor(self.adresseLED(x, y), 0, 0, 255)
        elif color == "DANGER":
            self.strand.setPixelColor(self.adresseLED(x, y), 255, 0, 0)
        else:
            self.strand.setPixelColor(self.adresseLED(x, y), 0, 0, 0)

    def dessiner(self) -> None:
        """Afficher le jeu sur les LEDs !"""

        # Récupérer la liste des coordonnées des missiles.
        missiles = self.modes.coordonnees_missiles()

        # Pour un jeu avec LEDs, on affiche tous les objets dessus.
        if self.modes.SUR_LEDS:

            self.afficherSurLed(
                self.food.xfood, self.food.yfood, color="FOOD")  # Food

            for snake in self.joueurs:
                for (x, y) in snake.positions:
                    self.afficherSurLed(x, y, color=snake.color)  # Snake

            for (x, y) in missiles:
                self.afficherSurLed(x, y, color="DANGER")  # Missiles

            self.strand.show()

        else:
            # Sans LEDs, on récupère les listes de positions des joueurs.
            pos1 = self.joueurs[0].positions if self.modes.NB_JOUEURS >= 1 else None
            pos2 = self.joueurs[1].positions if self.modes.NB_JOUEURS == 2 else None

            # On créé un dictionnaire contenant toutes les coordonnées des objets.
            game_status = {
                "pos1": pos1,
                "pos2": pos2,
                "food": [(self.food.xfood, self.food.yfood)],
                "bombes": self.modes.bombes_food,
                "missiles": missiles,
            }
            # Mettre à jour la simulation avec le dictionnaire.
            self.pseudo_leds.update(game_status)

    def condition_mort(self, snake) -> bool:
        """Déterminer si un snake donné doit mourir."""
        # Définition des parties tête et corps du snake.
        snake_body = snake.positions[:-3]
        snake_head = snake.positions[-3:]
        obstacles = self.modes.coordonnees_missiles() + self.modes.bombes_food

        # - Le snake se touche lui-même.
        touche_snake = any([head in snake_body for head in snake_head])
        # - Le snake touche un obstacle.
        touche_obstacle = any([obs in snake.positions for obs in obstacles])
        # - Le snake sort du plan (gauche ou droite).
        sorti_du_plan = snake.positions[-1][0] < 0 or snake.positions[-1][0] > self.width

        return touche_snake or touche_obstacle or sorti_du_plan

    def mort(self, snake_mort) -> None:
        """Gérer la mort d'un snake."""

        # Avec LEDs, on désactive la manette associée au snake,
        # et on joue une animation.
        if self.modes.SUR_LEDS:
            temps_proportionnel = round(3/(2*len(snake_mort.positions[:-1])), 4) # Pour fixer le temps de l'animation à 3 seconde.
            for c in ['DANGER', None]:
                for pix in snake_mort.positions[:-1][::-1]:
                    self.afficherSurLed(pix[0], pix[1], color=c)
                    self.strand.show()
                    time.sleep(temps_proportionnel)

        # Retirer le snake de la liste des joueurs ...
        self.joueurs_en_vie[snake_mort.snake_id] = False
        self.joueurs.remove(snake_mort)
        # ...et décrémenter le nombre de joueurs.
        self.modes.NB_JOUEURS -= 1

        # TODO : si le jeu est en coop et que le premier joueur meurt,
        # alors le snake mort reste a l'écran comme obstacle.

        # Dans le cas où aucun joueur n'est restant,
        if self.joueurs == []:

            if self.modes.SUR_LEDS:
                for i in ['SNAKE', 'FOOD', 'DANGER']*3:
                    self.afficherSurLed(self.food.xfood, self.food.yfood, i)
                    self.strand.show()
                    time.sleep(0.1)

                self.afficherSurLed(self.food.xfood, self.food.yfood, None)
                self.strand.show()

                # On révèle la position des cases mortelles.
                explosives = self.modes.bombes_food + self.modes.coordonnees_missiles()
                for x, y in explosives:
                    self.afficherSurLed(x, y, "DANGER")
                self.strand.show()

                time.sleep(1)

                # On éteint toutes les LEDs du panneau.
                for i in range(self.num_led):
                    self.strand.setPixelColor(i, 0, 0, 0)
                self.strand.show()

                # Quitter pygame.
                pygame.quit()

                # Stop le moteur.
                self.p.stop()
                GPIO.cleanup()
            else:
                # Avec la simulation, on arrête le subprocess pour les entrées clavier.
                self.listener.stop()

    def snake_mange_food(self, snake, xfood: int, yfood: int) -> bool:
        """Gérer les interaction entre le snake et la nourriture."""
        # Si la tête du snake est sur une case de nourriture.
        if snake.positions[-1] == [xfood, yfood]:
            # La nourriture n'est plus présente.
            self.food.food_presente = False
            # Le joueur gagne un point.
            self.score += 1
            # Si le snake à mangé une nourriture, alors il faudra ajouter un pixel au snake.
            return True
        else:
            return False

    def key_press_clavier(self, key):
        """Gérer les entrées clavier pour diriger les snakes."""
        for snake in self.joueurs:
            # Pour le 1er joueur, les touches sont 'zqsd',
            # soit haut, gauche, bas et droit, respectivement.
            if snake.snake_id == 0:
                for c, i in [('z', 0), ('d', 1), ('s', 2), ('q', 3)]:
                    # On regarde si la touche est pressée et,
                    # si le snake peut y aller, alors la touche est comptée comme active.
                    if (key.char == c) and i in snake.dir_possible:
                        snake.keys[i] = True

            # Pour le 2e joueur, les touches sont
            # 'oklm', soit haut, gauche, bas et droit, respectivement.
            else:
                for c, i in [('o', 0), ('m', 1), ('l', 2), ('k', 3)]:
                    # Idem...
                    if (key.char == c) and i in snake.dir_possible:
                        snake.keys[i] = True

    def key_press_pygame(self) -> None:
        """Gérer les entrées manette pour diriger le snake."""
        self.manettes: list[pygame.joystick.Joystick]
        for event in pygame.event.get():
            if event.type == pygame.JOYAXISMOTION:
                # manette 0
                if event.joy == 0 and self.joueurs_en_vie[0] == True:
                    if event.axis == 0:
                        if self.manettes[0].get_axis(event.axis) < -0.5 and 1 in self.joueurs[0].dir_possible:
                            self.joueurs[0].keys[1] = True  # GAUCHE
                        elif self.manettes[0].get_axis(event.axis) > 0.5 and 3 in self.joueurs[0].dir_possible:
                            self.joueurs[0].keys[3] = True  # DROITE
                    elif event.axis == 1:
                        if self.manettes[0].get_axis(event.axis) < -0.5 and 0 in self.joueurs[0].dir_possible:
                            self.joueurs[0].keys[0] = True  # HAUT
                        elif self.manettes[0].get_axis(event.axis) > 0.5 and 2 in self.joueurs[0].dir_possible:
                            self.joueurs[0].keys[2] = True  # BAS

                # manette 0
                elif event.joy == 1 and self.joueurs_en_vie[1] == True:
                    if event.axis == 0:
                        if self.manettes[1].get_axis(event.axis) < -0.5 and 1 in self.joueurs[1].dir_possible:
                            self.joueurs[1].keys[1] = True  # GAUCHE
                        elif self.manettes[1].get_axis(event.axis) > 0.5 and 3 in self.joueurs[1].dir_possible:
                            self.joueurs[1].keys[3] = True  # DROITE
                    elif event.axis == 1:
                        if self.manettes[1].get_axis(event.axis) < -0.5 and 0 in self.joueurs[1].dir_possible:
                            self.joueurs[1].keys[0] = True  # HAUT
                        elif self.manettes[1].get_axis(event.axis) > 0.5 and 2 in self.joueurs[1].dir_possible:
                            self.joueurs[1].keys[2] = True  # BAS

            if event.type == pygame.JOYBUTTONDOWN and event.button == 4:
                self.p.ChangeDutyCycle(config.ROTATION_BAS)
            elif event.type == pygame.JOYBUTTONDOWN and event.button == 5:
                self.p.ChangeDutyCycle(config.ROTATION_HAUT)
            elif event.type == pygame.JOYBUTTONUP and event.button == 4:
                self.p.ChangeDutyCycle(0)
            elif event.type == pygame.JOYBUTTONUP and event.button == 5:
                self.p.ChangeDutyCycle(0)

    def mainloop(self):
        """Code principal du jeu."""

        # Tant que des joueurs sont restants.
        while self.joueurs != []:
            # Attendre x seconde (Vitesse du jeu).
            time.sleep(self.vitesse)

            # Modes :
            # - Incrémentation du compteur de tour.
            self.modes.TEMPO_update()

            # - Missiles : Des missiles de 1x1 traversent l'écran aléatoirement.
            if self.modes.DIFF >= 1:
                self.modes.mode_missiles()

            # - Blink : La food change de position après x ticks, par défaut, 20.
            if self.modes.DIFF >= 2:
                self.modes.mode_blink(self.food.xfood, self.food.yfood)

                # - Bombes : L'ancienne position de la food devient dangereuse.
                if self.modes.DIFF >= 3:
                    self.modes.mode_bombes(self.food.xfood, self.food.yfood)

            # - Hidden : La food disparait après x ticks, par défaut, 15.
            if self.modes.DIFF > 3:
                self.modes.mode_hidden(self.food.xfood, self.food.yfood)

            # Récupérer la direction choisie par le joueur.
            if self.modes.SUR_LEDS:
                self.key_press_pygame()

            # Joueurs :
            for snake in self.joueurs:

                # Déterminer s'il faut garder le pixel de la nourriture.
                # <=> si le snake mange une nourriture.
                garder_food_pixel = self.snake_mange_food(
                    snake, self.food.xfood, self.food.yfood)

                # Mise à jour du snake.
                snake.direction_update()
                snake.deplacement(direction=snake.direction,
                                  garder_food_pixel=garder_food_pixel)

                # Mise à jour de la nourriture.
                self.food.mise_a_jour()

                # Si une condition d'arrêt est vraie, alors le snake meurt.
                if self.condition_mort(snake=snake):
                    self.mort(snake_mort=snake)

                # Sinon, afficher le jeu.
                else:
                    self.dessiner()

        # Si la boucle se termine, le jeu l'est aussi, on renvoie alors le score.
        return print(self.score)

    def initMoteur(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(config.MOTOR_PIN, GPIO.OUT)
        self.p = GPIO.PWM(config.MOTOR_PIN, config.MOTOR_HZ)
        self.p.start(0)  # Initialization


class Snake:
    """Classe représentant un Snake, dirigé par un joueur.

    Attributs:
    ----------
    - `game` : GAME
        - L'instance du jeu dans lequel le Snake est créé.
    - `snake_id` : int
        - Identificateur unique du Snake.

    - `positions` : list[list[int, int]] 
        - Les coordonnées occupées par le Snake.
    - `direction` : int[0,4]
        - La direction du Snake.
    - `dir_possible` : list
        - Les directions que le Snake peut prendre en fonction de sa direction actuelle.
    - `keys` : list[bool]
        - L'état de chaque direction. True si le joueur souhaite la prendre, false sinon.

    Méthodes:
    ---------
    - ``deplacement(direction:int, garder_food_pixel:bool)`` -> None:
        - Déplace le Snake dans la direction donnée. 
        Si garder_food_pixel est True, le Snake ne mange pas la nourriture et garde sa dernière case.

    - ``direction_update()`` -> None:
        - Met à jour la direction du Snake en fonction de l'état de l'attribut keys.
    """
    id = 0

    def __init__(self, game: GAME) -> None:
        # Cf. 'Snake.__doc__' pour la définition des attributs.
        self.game = game
        self.snake_id, Snake.id = Snake.id, Snake.id+1
        self.color = 'SNAKE'
        self.positions = [[0, 0], [1, 0], [2, 0]] if self.snake_id == 1 else [
            [0, 21], [1, 21], [2, 21]]  # Positions par défaut
        self.direction = 1  # Vers la droite par défaut.
        self.dir_possible = [(self.direction+1) % 4, (self.direction+3) % 4]
        self.keys = [False, False, False, False]

    def deplacement(self, direction: int, garder_food_pixel: bool) -> None:
        """Gérer un déplacement du snake.

        Args:
            direction (0-3): Direction du snake.
            garder_food_pixel (bool): Enlever ou non le 1er élément des positions.
        """
        # Si le snake n'a pas mangé de nourriture, eteindre la dernière led.
        if not garder_food_pixel:
            if self.game.modes.SUR_LEDS:
                self.game.afficherSurLed(
                    self.positions[0][0], self.positions[0][1], None)
                # self.game.strand.show()
            self.positions.pop(0)

        # En fonction de la direction, on ajoute la nouvelle coordonnée de la tete du snake.
        # - En allant vers la gauche, x devient plus grand
        # - En allant vers le bas, y devient plus grand
        if direction == 0:  # HAUT
            # seulement la coordonnée y change : y+1
            self.positions.append([self.positions[-1][0]])
            self.positions[-1].append(self.positions[-2][1]+1)

        elif direction == 2:  # BAS
            # seulement la coordonnée y change : y-1
            self.positions.append([self.positions[-1][0]])
            self.positions[-1].append(self.positions[-2][1]-1)

        elif direction == 1:  # DROITE
            # seulement la coordonnée x change : x+1
            self.positions.append([self.positions[-1][0]+1])
            self.positions[-1].append(self.positions[-2][1])

        elif direction == 3:  # GAUCHE
            # seulement la coordonnée x change : x-1
            self.positions.append([self.positions[-1][0]-1])
            self.positions[-1].append(self.positions[-2][1])

        # Si le snake sort du plan (haut ou bas),
        # réajuster sa position comme le plan est infini.
        if self.positions[-1][1] < 0:
            self.positions[-1][1] = self.game.height-1

        if self.positions[-1][1] > self.game.height-1:
            self.positions[-1][1] = 0

    def direction_update(self) -> None:
        """Mettre à jour la direction du snake en fonction de l'état de l'attribut keys."""

        # Si une touche est pressée,
        for x in range(len(self.keys)):
            if self.keys[x] == True:
                # Changer à la direction associée,
                self.direction = x
                # Actualiser la liste des directions possibles.
                self.dir_possible = [(self.direction+1) %
                                     4, (self.direction+3) % 4]
                # Désactiver la touche
                self.keys[x] = False


class Food:
    """Une classe représentant une nourriture dans le jeu.

    Attributs
    ---------
    - `game` : GAME
        - Parent de la nourriture, le jeu depuis lequel elle est appelée.
    - `food_presente` : bool
        - État de la nourriture sur l'écran : présente ou non.
    - `xfood`, `yfood` : int,int
        - Coordonnées de la nourriture.

    Méthodes
    --------
    - ``mise_a_jour()`` -> None
        - Met à jour la nourriture en fonction de l'attribut 'food_presente'.
    """

    def __init__(self, game: GAME) -> None:
        # Cf. 'Food.__doc__' pour la définition des attributs.
        self.game = game
        self.food_presente = False
        self.xfood, self.yfood = 0, 0

        # 1ère génération de nourriture.
        self.mise_a_jour()

    def mise_a_jour(self) -> None:
        """Mise à jour de la nourriture en fonction de l'attribut 'food_presente'."""
        if self.food_presente == False:
            # Génération aléatoire de coordonnées
            self.xfood = random.randint(0, self.game.width-1)
            self.yfood = random.randint(0, self.game.height-1)
            self.game.modes.TEMPO_tour = 0  # Réinitialisation du compteur.
            self.food_presente = True


class Missile:
    """Une classe représentant un missile dans le jeu.

    Attributes:
    ----------
    - `x` : int
        - La position horizontale du missile sur le terrain.
    - `y` : int
        - La position verticale du missile sur le terrain.
    - `sens` : int
        - La direction du missile, soit 1 pour aller vers la droite et -1 pour aller vers la gauche.

    Methods:
    -------
    - ``position_invalide()`` -> bool
        - Détermine si le missile est arrivé de l'autre côté du terrain, selon son sens.
    - ``deplacer()`` -> None:
        - Déplace le missile d'une case dans la direction spécifiée par l'attribut `sens`.
    """

    def __init__(self, x: int, y: int, sens: int) -> None:
        self.x, self.y = x, y
        self.sens = sens

    def position_invalide(self) -> bool:
        """Déterminer si le missile est arrivé de l'autre coté du terrain, selon son sens."""
        return self.sens == -1 and self.x <= 0 \
            or (self.sens == 1 and self.x >= 21)

    def deplacer(self) -> None:
        """Déplacer un missile."""
        self.x += self.sens


class Modes:
    """
    Gère les modes d'une partie.

    Attributs
    ---------
    - `game` : GAME
        - Parent, le jeu depuis lequel l'instance est appelée.
    - `SUR_LEDS` : bool
        - Mode de lancement du jeu : Avec LEDs, ou avec la simulation.
    - `DIFF` : int
        - Difficultée de la partie, choisie par le joueur.
    - `NB_JOUEURS` : int
        - Nombre de joueurs dans la partie.
    - `MULTI` : bool
        - Définition du type de partie multijoueur choisie.
    - `TEMPO_tour` : int
        - Compteur du nombre de tours.
    - `missiles`, `bombes_food` : list[list[int,int]]
        - Listes de coordonnées des missiles et des bombes.
    - `depart_par_sens` : dict
        - Association du sens d'un missile à sa position initiale.


    Méthodes
    --------
    - ``TEMPO_update()`` -> None:
        - Incrémentation du compteur de tours.
    - ``mode_blink(xfood:int, yfood:int)`` -> None
        - Déplacer la nourriture au bout d'un nombre de tours aléatoire.
    - ``mode_bombes(xfood:int, yfood:int)`` -> None
        - Transformer la position de la nourriture en une bombe au bout d'un nombre de tours aléatoire.
    - ``mode_hidden(xfood:int, yfood:int)`` -> None
        - Faire disparaitre la nourriture 15 tours après son apparition.
    - ``mode_missiles()`` -> None:
        - Déplacer/Créer des missiles qui traversent le terrain.
    - ``supprimer_missile(m:Missile)`` -> None
        - Supprimer un missile donné.
    - ``coordonnees_missiles()`` -> list[list[int,int]]
        - Récupérer les coordonnées de tous les missiles.

    Options possibles
    -----------------
    - Missiles : Des missiles de 1x1 traversent l'écran aléatoirement.
    - Blink    : La nourriture change de position au bout d'un nombre de tours aléatoire.
    - Bombes   : La nourriture se transforme en bombe au bout d'un nombre de tours aléatoire.
    - Hidden   : La nourriture disparait 15 tours après son apparition.

    Difficultés
    ------------
    - S++ : Missiles, Blink, Bombe, Hidden 
    - S   : Missiles, Blink, Bombe 
    - A   : Missiles, Blink
    - B   : Missiles
    - D   : Aucun mode activé.
    """

    def __init__(self, game: GAME, options_jeu: tuple):
        # Cf. 'Modes.__doc__' pour la définition des attributs.
        self.game = game
        dico_difficultes = {'D': 0, 'B': 1, 'A': 2, 'S': 3, 'S++': 99}
        self.SUR_LEDS = config.SUR_LEDS
        self.DIFF = dico_difficultes[options_jeu[0]]
        self.NB_JOUEURS = 1 + int(bool(options_jeu[1]))
        self.MULTI = True if options_jeu[1] == 'M' else False
        self.TEMPO_tour = 0
        self.missiles = []
        self.bombes_food = []
        self.depart_par_sens = {1: 0, -1: 21}

    def TEMPO_update(self) -> None:
        "Incrémentation du compteur de tours."
        self.TEMPO_tour += 1

    def mode_blink(self, xfood: int, yfood: int) -> None:
        """Déplacer la nourriture au bout d'un nombre de tours aléatoire."""
        if self.TEMPO_tour % random.choice([41, 43, 44, 48, 52, 55, 60, 77, 100]) == 0:
            # Demande de réinitialisation de la nourriture.
            self.game.food.food_presente = False
            # Supression de la nourriture sur le panneau.
            if self.SUR_LEDS:
                self.game.afficherSurLed(xfood, yfood, None)

    def mode_bombes(self, xfood: int, yfood: int) -> None:
        """Transformer la position de la nourriture en une bombe."""
        if self.TEMPO_tour % random.choice([19, 21, 23, 27, 31]) == 0:
            # Si la nourriture n'est pas encore dans les bombes,
            if [xfood, yfood] not in self.bombes_food:
                # Demande de réinitialisation de la nourriture.
                self.game.food.food_presente = False
                # Ajout dans la liste des bombes
                self.bombes_food.append([xfood, yfood])
                if self.SUR_LEDS:
                    # Affichage en rouge.
                    self.game.afficherSurLed(xfood, yfood, "DANGER")

    def mode_hidden(self, xfood: int, yfood: int) -> None:
        """Faire disparaitre la nourriture 15 tours après son apparition."""
        if self.TEMPO_tour % 15 == 0:
            # La nourriture et les bombes disparaissent du panneau.
            for i in ([[xfood, yfood]]+self.bombes_food):
                if self.SUR_LEDS:
                    self.game.afficherSurLed(i[0], i[1], None)

    def mode_missiles(self) -> None:
        """Gérer les missiles de la partie."""
        self.missiles: list[Missile]

        # Mise à jour de tous les missiles.
        for m in self.missiles:
            # Si un missile à terminé son parcours, on l'enlève.
            if m.position_invalide():
                self.supprimer_missile(m)
            # Sinon, on le déplace.
            else:
                self.deplacer_missile(m)

        # Créer des missiles à 20% de chance.
        if random.randint(1, 5) == 1:
            # Une chance sur deux pour son sens de parcours.
            sens = random.choice([-1, 1])
            self.missiles.append(Missile(
                self.depart_par_sens[sens], random.randint(1, self.game.height-1), sens))

    def supprimer_missile(self, m: Missile) -> None:
        """Supprimer un missile donné."""
        if self.game.modes.SUR_LEDS:
            # Supprimer l'affichage sur LEDs du missile.
            self.game.afficherSurLed(m.x, m.y, None)
        # Supression de la liste des missiles.
        self.missiles.remove(m)

    def deplacer_missile(self, m: Missile):
        """Déplacer un missile."""
        if self.game.modes.SUR_LEDS:
            # Supprimer l'affichage sur LEDs du missile.
            self.game.afficherSurLed(m.x, m.y, None)
        m.deplacer()

    def coordonnees_missiles(self) -> list[list[int, int]]:
        """Récupérer les coordonnées de tous les missiles."""
        return [[m.x, m.y] for m in self.missiles]


def SNAKEGAME():
    """Lancer le jeu avec les paramètres indiqués."""
    if sys_argv[1:]:

        option = sys_argv[1:]
        difficulte, mode = option
        game = GAME((difficulte, mode))
    else:
        game = GAME()
    game.mainloop()


if __name__ == "__main__":
    SNAKEGAME()
    # game = GAME((False, 'B',''))
    # print(game.mainloop())
