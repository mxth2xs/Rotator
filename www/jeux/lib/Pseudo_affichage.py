import pygame as pg
class Pseudo_leds:
    """Créer une simulation du panneau de LEDs.

    Attributs:
    ----------
    - `score` : int
        - Score de la partie en cours.
    - `panneau` : list
        - Ensemble des cases du panneau simulé.
    - `colors` : dict
        - Définition des couleurs utilisées.
    - `nb_leds` : list
        - Nombre de LEDs en largeur et hauteur sur le panneau.
    - `taille_fenetre` : tuple
        - Largeur et hauteur du panneau, en pixels (chaque case = 20 pixels.)
    - `window` : pygame.Surface
        - Fenêtre du jeu.

    Méthodes:
    ---------
    - ``set_led()`` -> None
        - Initialisation du panneau de LED.
    - ``resize_fenetre()`` -> None
        - Redimentionner la fenêtre si nécessaire.    
    - ``update(light_on:dict[str, list])`` -> None
        - Mise à jour de l'affichage en fonction des coordonnées des objets donnés.
    """
    panneau = []
    colors={
        'rouge': (200,0,0), 
        'vert':(0,200,0), 
        'bleu':(0,0,200), 
        'violet':(255,0,255), 
        'gris_foncé':(50,50,50), 
        'gris':(100,100,100),
        'vert_foncé':(0,100,0)
    }
    
    def __init__(self,nb_leds=(22,22)) -> None:
        """Initialisation de la classe Pseudo_leds.

        Args:
            nb_leds (tuple): Dimensions du panneau de LED (nombre de LED en largeur et en hauteur).
        """
        self.taille_une_case = 7

        self.nb_leds = list([n+1 for n in nb_leds]) 
        self.taille_fenetre=(self.nb_leds[0]*self.taille_une_case,self.nb_leds[1]*self.taille_une_case) 
        
        # Initialisation du module pygame
        pg.init()
        self.window = pg.display.set_mode(self.taille_fenetre,pg.RESIZABLE) # Création de la fenêtre de jeu
        pg.display.set_caption('Simulation LEDs') # Titre de la fenêtre de jeu
        self.set_led() # Initialisation du panneau de LED

    def set_led(self) -> None:
        """Initialisation du panneau de LED."""
        # Calcul de la largeur (l) et de la hauteur (h) de chaque pixel 
        # en fonction de la taille ²de l'écran et du nombre de leds.
        l = h = self.taille_une_case
        # On parcours le plan, et initialise dans chaque case du panneau, un rectangle de dimentions lxh.
        Pseudo_leds.panneau = [[pg.Rect(x*l,y*h,l,h) for y in range(self.nb_leds[1])] for x in range(self.nb_leds[0])]

 
    def update(self,light_on:dict[str, list]) -> None:
        """
        Mise à jour de l'affichage en fonction des coordonnées des objets donnés.

        Args:
            dico_LEDs_a_allumer (dict[str, list]): Dictionnaire répertoriant les objets et les coordonnées qu'ils occupent sur le panneau.
        """
        
        # Dessiner un rectangle noir dans toutes les cases du panneau.
        for colonne in Pseudo_leds.panneau:
            for case in colonne:
                pg.draw.rect(self.window,(0,0,0),case)
        
        # On parcours le dictionnaire donné dans lequel se trouve 
        # tous les objets qui doivent être affichés.
        for obj,pos_led in light_on.items():

            elements = {
                # GENERAL
                'pos1':'vert',
                'pos2':'violet',

                # SNAKE
                'food':'bleu',

                # SNAKE/JUMPMAN
                'missiles':'rouge', 
                'bombes':'rouge',
                
                # JUMPMAN
                'map':'rouge',
                'aerial_map':'bleu',
                'missiles_jump':'violet',

                # LABY
                'murs':'gris_foncé', 
                'fin':'bleu', 
                'solution':'vert_foncé', 
                'recherche':'gris'
            }
            
            if obj in elements and pos_led:
                for i in pos_led:
                    pg.draw.rect(self.window,Pseudo_leds.colors[elements[obj]],Pseudo_leds.panneau[i[0]][-(i[1]+1)]) #indice négatif car y=0 est en bas

        pg.event.get()
        pg.display.flip()
