class Servo():
    def __init__(self):
        RPi.GPIO.setmode(GPIO.BCM)  
        RPi.GPIO.setup(13, GPIO.OUT, initial=False)
        self.p = RPi.GPIO.PWM(13,50) #50HZ  
        self.p.start(0)  


    def tourne(self, value):
        self.p.ChangeDutyCycle(value)

    