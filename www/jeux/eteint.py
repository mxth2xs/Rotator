import config as config
def eteint():
    for i in range(config.NUM_LED):
        config.strand.setPixelColor(i, 0, 0 ,0)

    config.strand.show()

if __name__ == "__main__":
    eteint()
