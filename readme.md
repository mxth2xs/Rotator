# ROTATOR

## Lien vers la documentation

Vous pouvez trouver la documentation de ROTATOR prévue pour les Trophées NSI en suivant ce lien : [/doc/readme_Trophee_NSI.pdf](/doc/readme_Trophee_NSI.pdf)

Sinon, la documentation sans la présentation "normée" est disponible ici : [/doc/readme_sans.pdf](/doc/readme_sans.pdf)

## Lien vers la vidéo de présentation
[https://www.youtube.com/watch?v=oTMsJiUZsH0](https://www.youtube.com/watch?v=oTMsJiUZsH0)


## PRIX Trophées NSI
🏆 Meilleur projet de terminale en Île-de-France : Paris - Lauréat régional
[https://trophees-nsi.fr/resultats-2023](https://trophees-nsi.fr/resultats-2023)

- [Onepoint, membre du jury](https://www.groupeonepoint.com/fr/actualites/onepoint-membre-du-jury-des-trophees-nsi-lyceens/)
- [Onepoint, retour sur l'édition](https://www.linkedin.com/posts/onepoint_trophéensi-trophéesnsi-opportunité-activity-6940589804736675840-obSv/?originalSubdomain=fr)
- [Académie de Paris, retour sur l'édition 2023](https://pia.ac-paris.fr/portail/jcms/p2_2964820/trophees-nsi-retour-sur-l-edition-2023)
- [Louis-le-Grand, retour sur l'édition 2023](https://louislegrand.fr/2023/05/22/trophees-nsi-2023/)

