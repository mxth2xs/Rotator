import importlib
import rotator.algo.config
for lib in rotator.algo.config.modnames:
    globals()[lib] = importlib.import_module(lib)

def image_maker(*args):
    global img,auto

    def run_normal():
        # +-------- Redimension de l'image (22x22) -------+ #
        # - Si l'image a déjà les bonnes dimensions, on demande directement la liste de couleurs.
        if img.size == (22,22):
            liste_colors = list(img.getdata())
            imgSmall = img
        
        # - Sinon, on redimensionne et on demande la liste...
        else:
            imgSmall = img.resize((22,22),resample=PIL.Image.NEAREST)    
            liste_colors = list(imgSmall.getdata())

        #- Enlever la valeur de la transparence si 'png' (RGBA -> RGB).
        if img.mode == 'RGBA':
            for color in range(len(liste_colors)):
                liste_colors[color] = liste_colors[color][:-1]
        # +-----------------------------------------------+ #
        
        # +--------------- Afficher l'image --------------+ #
        # - Afficher sur PC (optionnel)
        imgSmall.show()

        # - Afficher sur LEDs 
        for led in range(rotator.algo.config.NUM_LED):
            rotator.algo.config.strand.setPixelColor(led, liste_colors[led][0], liste_colors[led][1], liste_colors[led][2])
            rotator.algo.config.strand.show()
        # +-----------------------------------------------+ #

    def pixel_art(list_color):
        rotator.algo.config.strand.show()
        for y in range(len(list_color)):
            for x in range(len(list_color[y])):
                
                if list_color[y][x][0] != "x":
                    rotator.algo.config.strand.setPixelColor(int(list_color[y][x][0]), int(list_color[y][x][1]), int(list_color[y][x][2]), int(list_color[y][x][3]))
                    rotator.algo.config.strand.show()

    # +---------------- Import d'image ---------------+ #
    try:
        if args[0] == False:
            tkinter.Tk().withdraw()
            filename = tkinter.filedialog.askopenfilename()
            img = PIL.Image.open(filename)
            run_normal()
        elif args[0] == True:
            img = PIL.Image.open( Path(str(Path(__file__).parents[1] / 'images/random')+ "\\" +random.choice(os.listdir(pathlib.Path(__file__).parents[1] / 'images/random'))))
            run_normal()
        elif args[0] == "death_snake":
            img = PIL.Image.open( Path(str(Path(__file__).parents[1] / 'images/death_snake')+ "\\" +random.choice(os.listdir(pathlib.Path(__file__).parents[1] / 'images/death_snake'))))
            run_normal()
        elif args[0] == "scribble_creation":
            img = PIL.Image.open( Path(str(Path(__file__).parents[1] / 'images/scribble_creation')+ "\\" + "IMAGE-" + str(len(os.listdir(pathlib.Path(__file__).parents[1] / 'images/scribble_creation'))) + ".jpg"))
            pixel_art(args[1])
        else:
            pass

    except:
        tkinter.Tk().withdraw()
        filename = tkinter.filedialog.askopenfilename()
        img = PIL.Image.open(filename)
        run_normal()
    # +-----------------------------------------------+ #

if __name__ == "__main__":
    image_maker(False)